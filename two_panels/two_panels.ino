#include <MemoryFree.h>

#include "FastSPI_LED2.h"
#define NUM_LEDS 295
CRGB leds[NUM_LEDS];

int randNum[NUM_LEDS];
int blinkCount;
int syncNum;
int nextBlinkCount;
unsigned long currentTime;
int runTimePassed;
byte sine;

byte mainBrightness    = 255;    // 0-255
int interval           = 5000;  // bellow 2000 faulty, below 1000 not working
byte r                 = 255;   // (0-127)
byte g                 = 255;   // (0-127)
byte b                 = 150;   // (0-127)
byte runTime           = 30; // intervals between sync


void setup() {
//  Serial.begin(9600);
  // LED library setup
  delay(2000); // sanity check
  LEDS.addLeds<WS2811, 13>(leds, NUM_LEDS);
  LEDS.setBrightness(mainBrightness);
  LEDS.show();

  blinkCount = 0;
  runTimePassed = runTime/2;
  nextBlinkCount = 1;
  syncNum = 0;
  currentTime = 0;
  
  for(int i=0; i < NUM_LEDS; i++) {
    randNum[i] = i;
  }
  shuffle(randNum, NUM_LEDS);  
  
} // end setup


void loop() {
  update();
  


  for (int i = 0; i < NUM_LEDS; i++) {

      leds[i] = CHSV(0,0,127*(sin(currentTime*TWO_PI/interval)+1));
    
//    sine = 127*(sin((currentTime+(randNum[i]/5*syncNum))*TWO_PI/interval)+1);
//    
//    if(sine < 2) {
//      leds[i] = CRGB(r,g,b);
//    } else {
//      leds[i] = CRGB(0,0,0);
//    }
//
  } // end for-loop

  LEDS.show();
  
} // end loop


// ----FUNCTIONS----

void update() {
  
  currentTime = millis();
  blinkCount = currentTime/interval;
   
  if(blinkCount < runTimePassed) {      ; 
      if (blinkCount == nextBlinkCount) {     
              syncNum++;
              nextBlinkCount++;
      } 
  
  } else {
          if (blinkCount == nextBlinkCount) { 
                syncNum--;
                nextBlinkCount++; 
          }
          if (syncNum == 0) {
                runTimePassed += runTime;
                shuffle(randNum, NUM_LEDS);
          }
  }
} // end update

void shuffle (int* randNum, int n)
  {
  while (--n >= 2)
    {
    // n is now the last pertinent index
    int k = random (n); // 0 <= k <= n - 1
    // Swap
    int temp = randNum[n];
    randNum[n] = randNum[k];
    randNum[k] = temp;
    }
}  // end of shuffle

